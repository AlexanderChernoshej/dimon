﻿namespace Dimon.Models.Layers.DataAccessLayer
{
    public interface IDataWriter
    {
        void Write(string fileName, string data);
    }
}
