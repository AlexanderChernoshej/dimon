﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;

namespace Dimon.Models.Layers.DataAccessLayer
{
    public class AppPackageFileReader : IDataReader
    {
        public List<string> Read(string fileName)
        {
            var output = new List<string>();

            using(var sReader = new StreamReader((Application.GetResourceStream(new Uri(fileName, UriKind.Relative))).Stream))
            {
                while (!sReader.EndOfStream)
                {
                    output.Add(sReader.ReadLine());
                }
            }

            return output;
        }
    }
}
