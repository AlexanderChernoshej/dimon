﻿using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;

namespace Dimon.Models.Layers.DataAccessLayer
{
    public class IsolatedStorageFileReader : IDataReader
    {
        public List<string> Read(string fileName)
        {
            List<string> output = new List<string>();

            using (var isolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (var streamReader = new StreamReader(isolatedStorage.OpenFile(fileName, FileMode.OpenOrCreate)))
                {
                    while (!streamReader.EndOfStream)
                    {
                        output.Add(streamReader.ReadLine());
                    }
                }
            }

            return output;
        }
    }
}
