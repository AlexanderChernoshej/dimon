﻿using System.Collections.Generic;

namespace Dimon.Models.Layers.DataAccessLayer
{
    public interface IDataReader
    {
        List<string> Read(string fileName);
    }
}
    