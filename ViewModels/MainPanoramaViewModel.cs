﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Dimon.Models.Layers.DataAccessLayer;
using Dimon.Models.Layers.DataLayer;
using Dimon.Resources;

namespace Dimon.ViewModels
{
    public class MainPanoramaViewModel : INotifyPropertyChanged
    {

        //private variables
        private static MainPanoramaViewModel _instance;
        private readonly List<string> _rhymesList;


        //properties and public variables   
        #region Singleton implementation
        public static MainPanoramaViewModel Instance
        {
            get { return _instance ?? (_instance = new MainPanoramaViewModel()); }
        }
        #endregion

        #region Interface to present model in view
        public ObservableCollection<string> GuessedList { get; private set; }        

        public int Guessed { get { return GuessedList.Count; }}

        public int RestToGuess { get { return _rhymesList.Count - Guessed; }}

        public int TotalRhymes { get { return _rhymesList.Count; }}

        public int Radius { get { return Guessed/3; }}

        public string OkMessage { get { return InputBoxMessages.Instance.OkMessage; } }
        
        public string WrongMessage { get { return InputBoxMessages.Instance.NotFoundMessage; } }
        
        public string ExistedMessage { get { return InputBoxMessages.Instance.ExistedMessage; } }

        public string Hint
        {
            get
            {
                foreach (var word in _rhymesList.Where(word => !GuessedList.Contains(word)))
                {
                    return word;
                }
                return string.Empty;
            }
        }
        #endregion

        //TODO replace somwhere to BusinesLogic
        public enum WordType
        {
            Ok,
            NotFound,
            Existed
        }
        public WordType CheckWord(string word)
        {
            if (!_rhymesList.Contains(word))
                return WordType.NotFound;
            if (GuessedList.Contains(word))
                return WordType.Existed;
            GuessedList.Insert(0,word);            
            return WordType.Ok;
        }

        //private methods
        private MainPanoramaViewModel()
        {
            //TODO inject dependencies to avoid this shit
            _rhymesList = new AppPackageFileReader().Read(AppResources.RhymesFileName);

            //TODO repair this very very bad code
            var guessedList = new IsolatedStorageFileReader().Read(AppResources.GuessedFileName);
            
            guessedList.Reverse();
            GuessedList = new ObservableCollection<string>(guessedList);

            GuessedList.CollectionChanged += GuessedList_CollectionChanged;            
        }

        void GuessedList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            foreach (var newItem in e.NewItems)
            {
                //TODO avoid strong coupling
                new IsolatedStorageFileWriter().Write(AppResources.GuessedFileName, newItem.ToString());                
                
                //TODO fix this missunderstanding
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs("Guessed"));
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs("Radius"));
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs("RestToGuess"));           
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}