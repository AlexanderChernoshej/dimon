﻿using System;
using System.IO.IsolatedStorage;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using Dimon.Resources;
using Dimon.ViewModels;
using Microsoft.Phone.Tasks;

namespace Dimon.Views
{
    public partial class MainPanorama
    {        
        public MainPanorama()
        {            
            InitializeComponent();                                     
            DataContext = MainPanoramaViewModel.Instance;                                  
        }        

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var settings = IsolatedStorageSettings.ApplicationSettings;
            if (settings.Contains("WasLaunched")) return;
            MessageBox.Show(AppResources.Introduction);
            settings.Add("WasLaunched", true);           
        }        
        

        private void InputTextBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            
            Focus();
            InputTextBox.Tap += InputTextBox_Tap;
            switch (MainPanoramaViewModel.Instance.CheckWord((sender as TextBox).Text))
            {
                case MainPanoramaViewModel.WordType.NotFound:
                    InputTextBox.Text = MainPanoramaViewModel.Instance.WrongMessage;
                    break;
                case MainPanoramaViewModel.WordType.Existed:
                    InputTextBox.Text = MainPanoramaViewModel.Instance.ExistedMessage;
                    break;
                case MainPanoramaViewModel.WordType.Ok:
                    InputTextBox.Text = MainPanoramaViewModel.Instance.OkMessage;
                    break;
            }
        }

        void InputTextBox_Tap(object sender, GestureEventArgs e)
        {
            InputTextBox.Text = string.Empty;
            InputTextBox.Tap -= InputTextBox_Tap;
        }

        private void GuessedTextBlock_OnTap(object sender, GestureEventArgs e)
        {
            var wikiConfirmResult = MessageBox.Show(AppResources.GuessTxtBlockAction, AppResources.Confirmation, MessageBoxButton.OKCancel);            

            if (wikiConfirmResult != MessageBoxResult.OK) return;

            var webBrowserTask = new WebBrowserTask
            {
                Uri = new Uri("https://ru.m.wikipedia.org/wiki/" + (sender as TextBlock).Text, UriKind.Absolute)
            };
            webBrowserTask.Show();
        }

        private void DimonImage_OnTap(object sender, GestureEventArgs e)
        {
            MessageBox.Show(AppResources.DimonImgMBText);
        }        

        private void HintButton_OnClick(object sender, RoutedEventArgs e)
        {
        }

        #region Helpers
        //private float Expand(float step)
        //{

        //}
        #endregion
    }
}