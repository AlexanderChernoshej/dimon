﻿using System.IO;
using System.IO.IsolatedStorage;

namespace Dimon.Models.Layers.DataAccessLayer
{
    public class IsolatedStorageFileWriter : IDataWriter
    {
        public void Write(string fileName, string data)
        {
            using (var isolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (var streamWriter = new StreamWriter(isolatedStorage.OpenFile(fileName,FileMode.Append)))
                {
                    streamWriter.WriteLine(data);
                }
            }
        }
    }
}
