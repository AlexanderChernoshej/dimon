﻿using System;

namespace Dimon.Models.Layers.DataLayer
{
    public class InputBoxMessages
    {
        private static InputBoxMessages _instance;
        private string[] _okMessages;
        private string[] _existedMessages;
        private string[] _wrongMessages;
        private Random _randomizer;


        public static InputBoxMessages Instance
        {
            get
            {
                return _instance ?? (_instance = new InputBoxMessages());
            }
        }

        public string OkMessage{get { return _okMessages[_randomizer.Next(0, _okMessages.Length)]; }}
        public string ExistedMessage { get{return _existedMessages[_randomizer.Next(0, _existedMessages.Length)]; } }
        public string NotFoundMessage { get { return _wrongMessages[_randomizer.Next(0, _wrongMessages.Length)]; } }

        private InputBoxMessages()
        {
            _randomizer = new Random();

            _okMessages = new[] {"збс :)", "ОК", "ты котик =*", "красава"};
            _wrongMessages = new[] {"не подходит", "неа", "соберись", "это неприлично" , "может возьмёте подсказку ?"};
            _existedMessages = new[] {"было", "было !", "БЫЛО !!!1", "повторяешься"};
        }
    }
}
